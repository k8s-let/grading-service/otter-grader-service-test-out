# Services for otter-grader-service Integration Tests

## otter-grader-test-out

Acts as receiving service. Evaluates the grading results of the three test submissions.

Reports the current test results back on `/status` as JSON:

```json
{
  "1":1,
  "2":1,
  "3":1,
  "summary":1}
```

The `summary` is set to 1 if all tests have passed.
